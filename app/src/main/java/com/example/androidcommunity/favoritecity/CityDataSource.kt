package com.example.androidcommunity.favoritecity

import android.annotation.SuppressLint
import com.example.androidcommunity.R

class CityDataSource {
        @SuppressLint("ResourceType")
        fun loadCities(): List<City> {
        return listOf(
            City(R.string.spain, R.drawable.spain),
            City(R.string.new_york, R.drawable.newyork),
            City(R.string.tokyo, R.drawable.tokyo),
            City(R.string.switzerland, R.drawable.switzerland),
            City(R.string.singapore, R.drawable.singapore),
            City(R.string.paris, R.drawable.paris),
        )
    }
}